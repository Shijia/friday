	<?php
$connect = mysqli_connect("localhost","root","110119");
if (!$connect)
	{
  	die('Could not connect: ' . mysqli_error());
  	}
$sql_create = "CREATE DATABASE Legislator";
if (mysqli_query($connect,$sql_create))
	{
  	echo "Database my_db created successfully\n";
  	}
	else
  	{
  	echo "Error creating database: " . mysqli_error($connect);
  	}
mysqli_select_db($connect, "Legislator");
$sql_create_user = "CREATE TABLE IF NOT EXISTS `User` (
	`User ID` int(9) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
  	`District Number` varchar(10) NOT NULL,
  	`County` varchar(50) NOT NULL,
  	PRIMARY KEY (`User ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1" ;

$sql_create_contact = "CREATE TABLE IF NOT EXISTS `Contact` (
	`Contact ID` int(9) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	`Annapolis Address` varchar(100) NOT NULL,
  	`Interim Address` varchar(100) NOT NULL,
  	`Phone Number(s)` varchar(20) NOT NULL,
  	`Contact` varchar(200) NOT NULL,
  	`Tenure` varchar(100) NOT NULL,
  	`Current Assignments` varchar(500) NOT NULL,
  	`Party Affiliation` varchar(20) NOT NULL,
  	PRIMARY KEY (`Contact ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1" ;

$sql_create_SL = "CREATE TABLE IF NOT EXISTS `Session Legislation` (
	`SL ID` int(9) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
  	`record` varchar(10000) NOT NULL,
  	PRIMARY KEY (`SL ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1" ;

$sql_create_Bio = "CREATE TABLE IF NOT EXISTS `Biography` (
	`Biography ID` int(9) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	`Past senate` varchar(3000) NOT NULL,
	`Past house` varchar(3000) NOT NULL,
  	`Public Service` varchar(3000) NOT NULL,
  	`Memberships` varchar(3000) NOT NULL,
  	`Awards` varchar(3000) NOT NULL,
  	`Biographical Information` varchar(3000) NOT NULL,
  	PRIMARY KEY (`Biography ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1" ;


mysqli_query($connect, $sql_create_user);
mysqli_query($connect, $sql_create_contact);
mysqli_query($connect, $sql_create_SL);
mysqli_query($connect, $sql_create_Bio);

$url_home = 'http://mgaleg.maryland.gov/webmga/';
$url = 'http://mgaleg.maryland.gov/webmga/frmmain.aspx?pid=legisrpage&tab=subject6';
function get_html($url_current)
	{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url_current);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$contents = curl_exec($ch);
	curl_close($ch);
	return $contents;
	}
function find_table_1($url_current)
	{
	$table_1 = array();
	$connect = mysql_connect("localhost","root","110119");
	$contents = get_html($url_current);
	$contents = str_replace("\r\n"," ",$contents);
	$find_sub = '/<td><a HREF="(.*?)">/';
	$find_name = '/RS">(.*?)</';
	$find_DN = '/<\/a><\/td><td class="hcenter">(.*?)<\/td><td>/';
	$find_county = '/<\/td><td>(.*?)<\/td><td/';
	preg_match_all($find_sub, $contents,$sub);
	preg_match_all($find_name, $contents, $name);
	preg_match_all($find_DN, $contents, $DN);
	preg_match_all($find_county, $contents, $county);
	mysql_select_db("Legislator", $connect);
	$table_1[0] = $sub[1];
	$table_1[1] = $name[1];
	$table_1[2] = $DN[1];
	$table_1[3] = $county[1];
	return $table_1;
	}
function insert_table_1($url)
	{
	echo "inserting table 1\n";
	$connect = mysqli_connect("localhost","root","110119","Legislator");
	$Name = "";
	$Number = "";
	$County = "";
	$table_1 = find_table_1($url);
	for ($i = 0; $i <= 188; $i++){
		$Name = $table_1[1][$i];
		$Number = $table_1[2][$i];
		$County = $table_1[3][$i];
		$Name = mysql_real_escape_string($Name);
		$Number = mysql_real_escape_string($Number);
		$County = mysql_real_escape_string($County);
		$sql_table_1= "INSERT INTO User (`User ID`, `name`, `District Number`, `County`) VALUES (NULL, '$Name', '$Number', '$County');";
		mysqli_query($connect, $sql_table_1);
		}
		
	}

function find_table_2($current_url, $url_home)
	{
	$table_1 = find_table_1($current_url);
	$SL_suffix = array();
	$Bio_suffix = array();
	$address = array();
	$Iaddress = array();
	$Phone = array();
	$contact = array();
	$Tenure = array();
	$CA = array();
	$PA = array();
	$contents_new = "";
	$SL_BIO = array();
	for ($i = 0; $i <=188; $i++)
		{	
		$url_nxt = $url_home . "" . $table_1[0][$i];
		$contents = get_html($url_nxt);
		$contents = str_replace("<br />","          ",$contents); 
		$contents = str_replace("\r\n"," ",$contents);
		$find_SL = '/id="stab2"  href="(.*?)" > <span/';
		$find_Bio = '/id="stab3"  href="(.*?)" > <span/';
		$find_AA = '/Annapolis Address:(.*?)Interim Address/';
		$find_IA = '/Interim Address:(.*?)Phone/';
		$find_number = '/Number\(s\)(.*?)Contact/';
		$find_C = '/Contact:(.*?)Tenure/';
		$find_T = '/Tenure:(.*?)Current/';
		$find_assign = '/Current Assignments:(.*?)Party Affiliation/';
		$find_PA = '/Party Affiliation:(.*?) /';
		preg_match($find_SL, $contents, $SL);
		array_push($SL_suffix, $SL[1]);
		preg_match($find_Bio, $contents, $Bio);
		array_push($Bio_suffix, $Bio[1]);
		$contents_new = strip_tags($contents);
		if (preg_match($find_AA, $contents_new, $AA)){
		array_push($address, $AA[1]);
		}
		else{
		array_push($address, "none");
		}
		if (preg_match($find_IA, $contents_new, $IA)){
		array_push($Iaddress, $IA[1]);
		}
		else{
		array_push($Iaddress, "none");
		}
		if (preg_match($find_number, $contents_new, $number)){
		array_push($Phone, $number[1]);
		}
		else{
		array_push($Phone, "none");
		}
		if (preg_match($find_C, $contents_new, $email)){
		array_push($contact, $email[1]);
		}
		else{
		array_push($contact, "none");
		}
		if (preg_match($find_T, $contents_new, $Ten)){
		array_push($Tenure, $Ten[1]);
		}
		else{
		array_push($Tenure, "none");
		}
		if (preg_match($find_assign, $contents_new, $assign)){
		array_push($CA, $assign[1]);
		}
		else{
		array_push($CA, "none");
		}
		if (preg_match($find_PA, $contents_new, $Party)){
		array_push($PA, $Party[1]);
		}
		else{
		array_push($PA, "none");
		}
		$table_2[0] = $SL_suffix;
		$table_2[1] = $Bio_suffix;
		$table_2[2] = $address;
		$table_2[3] = $Iaddress;
		$table_2[4] = $Phone;
		$table_2[5] = $contact;
		$table_2[6] = $Tenure;
		$table_2[7] = $CA;
		$table_2[8] = $PA;
		}
	return $table_2;
	}
function insert_table_2($url, $url_home)
	{
	echo "inserting table 2\n";
	$connect = mysqli_connect("localhost","root","110119","Legislator");
	$table_2 = find_table_2($url, $url_home);
	$AAddress = "";
	$IAddress = "";
	$Phone = "";
	$Contact = "";
	$Tenure = "";
	$current_assign = "";
	$Party_A = "";
	$table_1 = find_table_1($url);
	for ($i = 0; $i <= 188; $i++){
		$Name = $table_1[1][$i];
		$AAddress = $table_2[2][$i];
		$IAddress = $table_2[3][$i];
		$Phone = $table_2[4][$i];
		$Contact = $table_2[5][$i];
		$Tenure = $table_2[6][$i];
		$current_assign = $table_2[7][$i];
		$Party_A = $table_2[8][$i];
		$Name = mysql_real_escape_string($Name);
		$AAddress = mysql_real_escape_string($AAddress);
		$IAddress = mysql_real_escape_string($IAddress);
		$Phone = mysql_real_escape_string($Phone);
		$Contact = mysql_real_escape_string($Contact);
		$Tenure = mysql_real_escape_string($Tenure);
		$current_assign = mysql_real_escape_string($current_assign);
		$Party_A = mysql_real_escape_string($Party_A);
		$sql_table_2= "INSERT INTO Contact (`Contact ID`, `name`,  `Annapolis Address`, `Interim Address`, `Phone Number(s)`, `Contact`, `Tenure`, `Current Assignments`, `Party Affiliation`) VALUES(NULL, '$Name', '$AAddress', '$IAddress', '$Phone', '$Contact', '$Tenure', '$current_assign', '$Party_A');";
		mysqli_query($connect, $sql_table_2);
		}
	}

function find_table_3($url, $url_home)
	{
	$table_2 =  find_table_2($url, $url_home);
	$SL_suffix = $table_2[0];
	$SL_record = array();
	for ($i = 0; $i <=188; $i++)
		{
		$url_table3 = $url_home . "" . $SL_suffix[$i];
		$contents = get_html($url_table3);
		$contents = str_replace("<td>","  ",$contents);
		$contents = str_replace("\r\n"," ",$contents);
		$contents = str_replace("</div>","end_create",$contents);
		$contents_new = strip_tags($contents);
		$find_record = '/TypeBroad Subject(.*?)end_create/';
		if (preg_match($find_record, $contents_new, $SL)){
		array_push($SL_record, $SL[1]);
		}
		else{
		array_push($SL_record, "none");
		}
		}
	return $SL_record;
	}
	
function insert_table_3($url, $url_home)
	{
	echo "inserting table 3\n";
	$connect = mysqli_connect("localhost","root","110119", "Legislator");
	$table_1 = find_table_1($url);
	$Name = "";
	$SL_record = find_table_3($url, $url_home);
	for ($i = 0; $i <= 188; $i++){
		$Name = $table_1[1][$i];
		$SL = $SL_record[$i];
		$Name = mysql_real_escape_string($Name);
		$SL = mysql_real_escape_string($SL);
		$sql_table_3= "INSERT INTO `Session Legislation` (`SL ID`, `name`, `record`) VALUES (NULL, '$Name', '$SL');";
		mysqli_query($connect, $sql_table_3);
		}
	}

function find_table_4($url, $url_home)
	{
	$table_2 =  find_table_2($url, $url_home);
	$Bio_suffix = $table_2[1];
	$Past_senate = array();
	$Past_house = array();
	$Public_service = array();
	$membership = array();
	$Awards = array();
	$Biographical = array();
	for ($i = 0; $i <=188; $i++)
		{
		$url_table4 = $url_home . "" . $Bio_suffix[$i];
                $contents = get_html($url_table4);
		$contents = str_replace("\r\n"," ",$contents);
		$contents = str_replace("<th","next_one",$contents);
		$contents = str_replace("</div", "end", $contents);
		$contents_new = strip_tags($contents);
		$find_past_senate = '/Past Senate Service(.*?)next_one/';
		$find_past_house = '/Past House Service(.*?)next_one/';
		$find_public ='/Public Service(.*?)next_one/';
		$find_member = '/Memberships(.*?)next_one/';
		$find_Awards = '/Awards(.*?)next_one/';
		$find_Biographical = '/Biographical Information(.*?)end/';
		if (preg_match($find_past_senate, $contents_new, $past_s))
			{
			array_push($Past_senate, $past_s[1]);
			}
		else
			{
			array_push($Past_senate, "none");
			}
		if (preg_match($find_past_house, $contents_new, $past_h))
			{
			array_push($Past_house, $past_h[1]);
			}
			else{
			array_push($Past_house, "none");
			}
		if (preg_match($find_public, $contents_new, $public))
			{
			array_push($Public_service, $public[1]);
			}
		else
			{
			array_push($Public_service, "none");
			}
		if (preg_match($find_member, $contents_new, $member)){
			array_push($membership, $member[1]);
			}
		else{
			array_push($membership, "none");
			}
		if (preg_match($find_Awards, $contents_new, $awards)){
			array_push($Awards, $awards[1]);
			}
		else{
			array_push($Awards, "none");
			}
		if (preg_match($find_Biographical, $contents_new, $Biograph)){
			array_push($Biographical, $Biograph[1]);
			}
		else{
			array_push($Biographical, "none");
			}
		}
	$table_4 = array();
	$table_4[0] = $Past_senate;
	$table_4[1] = $Past_house;
	$table_4[2] = $Public_service;
	$table_4[3] = $membership;
	$table_4[4] = $Awards;
	$table_4[5] = $Biographical;
	return $table_4;
	}

function insert_table_4($url, $url_home)
	{
	echo "inserting table 4\n";
	$table_1 = find_table_1($url);
	$connect = mysqli_connect("localhost","root","110119", "Legislator");
	$table_4 = find_table_4($url, $url_home);
	$Name = "";
	$Senate = "";
	$House = "";
	$Service = "";
	$mem = "";
	$AW = "";
	$Bio = "";
	$Name = "";
	for ($i = 0; $i <= 188; $i++)
		{
		$Name = $table_1[1][$i];
		$Senate = $table_4[0][$i];
		$House = $table_4[1][$i];
		$Service = $table_4[2][$i];
		$mem = $table_4[3][$i];
		$AW = $table_4[4][$i];
		$Bio = $table_4[5][$i];
		$Senate = mysql_real_escape_string($Senate);
		$House = mysql_real_escape_string($House);
		$Service = mysql_real_escape_string($Service);
		$mem = mysql_real_escape_string($mem);
		$AW = mysql_real_escape_string($AW);
		$Bio = mysql_real_escape_string($Bio);
		$Name = mysql_real_escape_string($Name);
		$sql_table_4 = "INSERT INTO `Legislator`.`Biography` (`Biography ID`, `name`, `Past Senate`, `Past house`, `Public Service`, `Memberships`, `Awards`, `Biographical Information`) 
		VALUES 
		(NULL, '$Name', '$Senate', '$House', '$Service', '$mem', '$AW', '$Bio');";
		mysqli_query($connect, $sql_table_4);
		}
	}

insert_table_1($url);
insert_table_2($url, $url_home);
insert_table_3($url, $url_home);
insert_table_4($url, $url_home);
mysqli_close($connect);
?>
